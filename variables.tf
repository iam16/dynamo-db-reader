variable "dynamo_db_table_arn" {
  type        = string
  description = "arn of dynamo db table the policy allows read access to"
}

variable "dynamo_db_table_name" {
  type        = string
  description = "the name of the table the policy allows read access to"
}