resource "aws_iam_policy" "policy" {
  name        = join("-", [var.dynamo_db_table_name, "reader-policy"])
  description = join("-", [var.dynamo_db_table_name, "reader-policy"])

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:DescribeTable",
        "dynamodb:Query",
        "dynamodb:Scan",
        "dynamodb:GetItem"
      ],
      "Effect": "Allow",
      "Resource": "${var.dynamo_db_table_arn}"
    }
  ]
}
EOF
} 